import { useOutletContext } from "react-router-dom";

function Ingredient() {
  const { ingredients } = useOutletContext();

  return (
    <>
      <div className="overflow-x-auto">
        <table className="table">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
            {ingredients.map((i, index) => (
              <tr key={index}>
                <th>{index + 1}</th>
                <td className="capitalize">{i.name}</td>
                <td className="capitalize">{i.quantity}</td>
                <td>{i.type}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default Ingredient;

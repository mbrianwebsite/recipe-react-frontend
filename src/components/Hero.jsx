/* eslint-disable react/prop-types */

function Hero({ handleSearchRecipe, query, setQuery }) {
  const handleChangeQuery = (e) => {
    setQuery(e.target.value);
  };

  const handleInputEnter = (e) => {
    if (e.key === "Enter") {
      handleSearchRecipe(query);
    }
  };

  return (
    <>
      <div className="hero rounded-2xl bg-base-200">
        <div className="hero-content flex-col p-6 md:flex-row-reverse md:p-8">
          <img
            src="https://cdn.pixabay.com/photo/2017/07/28/14/29/macarons-2548827_1280.jpg"
            className="w-full rounded-lg shadow-2xl md:w-1/2"
          />
          <div>
            <h1 className="text-5xl font-bold">
              Cooking <span className="text-error">Made Simple</span>
            </h1>
            <p className="py-6">
              Whether you&apos;re a novice or a seasoned cook, our step-by-step
              guides make cooking a joyous experience.
            </p>
            <div className="join">
              <input
                type="text"
                placeholder="Type here"
                className="input join-item input-bordered w-full max-w-xs"
                onChange={handleChangeQuery}
                onKeyUp={handleInputEnter}
                value={query}
              />
              <button
                className="btn btn-primary join-item"
                onClick={() => handleSearchRecipe(query)}
              >
                Search
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Hero;

import { useOutletContext } from "react-router-dom";

function Instructions() {
  const { instructions } = useOutletContext();
  return (
    <>
      <div className="overflow-x-auto">
        <table className="table">
          <thead>
            <tr>
              <th>No</th>
              <th>Step</th>
            </tr>
          </thead>
          <tbody>
            {instructions.map((s, index) => (
              <tr key={index}>
                <th>{index + 1}</th>
                <td>{s}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default Instructions;

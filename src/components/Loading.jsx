import loadingGif from "../assets/loading.gif";

function Loading() {
  return (
    <div className="hero min-h-full bg-base-200">
      <div className="hero-content text-center">
        <div className="max-w-md">
          <img src={loadingGif} alt="loading..." className="scale-150" />
        </div>
      </div>
    </div>
  );
}

export default Loading;

import ProductCard from "./ProductCard";

function ProductGrid(recipesData) {
  const recipes = recipesData.recipesData;
  return (
    <>
      <div className="grid w-full grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-3">
        {recipes?.map((recipe) => (
          <ProductCard recipe={recipe} key={recipe.id} />
        ))}
      </div>
    </>
  );
}

export default ProductGrid;

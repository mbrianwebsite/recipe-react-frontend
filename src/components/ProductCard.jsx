/* eslint-disable react/prop-types */
import { useNavigate } from "react-router-dom";
function ProductCard({ recipe }) {
  const { id, name, imageURL, category, timers } = recipe;

  const navigate = useNavigate();

  const navigateToRecipe = (id) => {
    navigate("/recipe/" + id + "/ingredients");
  };
  return (
    <div
      className="onHover card bg-base-100 shadow-xl hover:scale-105"
      onClick={() => navigateToRecipe(id)}
    >
      <figure>
        <img src={imageURL} alt="Shoes" className="h-40 w-full object-cover" />
      </figure>
      <div className="card-body h-52 justify-between p-6 md:p-8">
        <h2 className="card-title">
          {name}
          {/* <div className="badge badge-secondary">NEW</div> */}
        </h2>
        {/* <p>If a dog chews shoes whose shoes does he choose?</p> */}
        <div className="card-actions justify-between pt-2">
          <div className="badge badge-primary p-3 font-medium">{category}</div>
          <div className="font-medium text-neutral">
            {timers.reduce((a, b) => a + b, 0)} Minutes
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductCard;

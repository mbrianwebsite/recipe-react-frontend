import axios from "axios";
import { useReducer } from "react";
import { useSearchParams } from "react-router-dom";

const initialState = {
  recipes: null,
  loading: false,
  error: null,
};

const ACTION = {
  FETCHING_DATA: "FETCHING_DATA",
  FETCH_SUCCESSFUL: "FETCH_SUCCESSFUL",
  FETCH_ERROR: "FETCH_ERROR",
};

function reducer(_, action) {
  switch (action.type) {
    case ACTION.FETCHING_DATA:
      return {
        recipes: null,
        error: null,
        loading: true,
      };
    case ACTION.FETCH_SUCCESSFUL:
      return {
        error: null,
        loading: false,
        recipes: action.payload,
      };
    case ACTION.FETCH_ERROR:
      return {
        error: action.payload,
        recipes: null,
        loading: false,
      };
    default:
      return initialState;
  }
}

const useFetchRecipes = (id) => {
  // const [recipes, setRecipes] = useState([]);
  // const [loading, setLoading] = useState(false);
  // const [error, setError] = useState(null);

  const [{ recipes, loading, error }, dispatch] = useReducer(
    reducer,
    initialState,
  );

  let getUrl;

  if (id) {
    getUrl = "http://localhost:3000/recipes/" + id;
  } else {
    getUrl = "http://localhost:3000/recipes";
  }

  let option = {
    url: getUrl,
    headers: {
      authenticated: "yes",
    },
  };

  const [searchParams] = useSearchParams();

  const fetchRecipes = async (searchQuery) => {
    dispatch({ type: ACTION.FETCHING_DATA });
    if (searchParams.get("q")) {
      option = { ...option, params: { q: searchParams.get("q") } };
    }
    if (searchQuery) {
      option = { ...option, params: { q: searchQuery } };
    }
    await axios(option)
      .then(function (response) {
        // handle success
        dispatch({ type: ACTION.FETCH_SUCCESSFUL, payload: response.data });
      })
      .catch(function (error) {
        // handle error
        dispatch({ type: ACTION.FETCH_ERROR, payload: error.response.data });
      })
      .finally(function () {
        // always executed
      });
  };

  return [fetchRecipes, { recipes, loading, error }];
};

export default useFetchRecipes;

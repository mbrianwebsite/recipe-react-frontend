import Navbar from "./components/Navbar";
import { Outlet } from "react-router-dom";

function App() {
  return (
    <main className="mx-auto flex max-w-5xl flex-col items-center gap-6 px-4 py-4 lg:gap-10">
      <Navbar />
      <Outlet />
    </main>
  );
}

export default App;

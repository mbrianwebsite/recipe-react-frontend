/* eslint-disable react/prop-types */

function ErrorPage({ message, explanation }) {
  return (
    <>
      <div className="hero min-h-screen bg-base-200">
        <div className="hero-content text-center">
          <div className="max-w-md">
            <h1 className="text-5xl font-bold">{message ? message : "404"}</h1>
            <p className="py-6">
              {explanation ? explanation : "Page not found!"}
            </p>
            <button className="btn btn-primary">Go to Home Page</button>
          </div>
        </div>
      </div>
    </>
  );
}
export default ErrorPage;

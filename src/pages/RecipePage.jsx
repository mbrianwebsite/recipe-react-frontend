import { useEffect, useState } from "react";
import { Outlet, useNavigate, useParams } from "react-router-dom";
import useFetchRecipes from "../hooks/useFetchRecipes";
import Loading from "../components/Loading";
import Error from "../components/Error";

function RecipePage() {
  const { id } = useParams();

  const [tabs, setTabs] = useState(1);

  const [fetchRecipes, { recipes, loading, error }] = useFetchRecipes(id);

  const navigate = useNavigate();

  useEffect(() => {
    fetchRecipes();
  }, []);

  const handleTabs = (tabKey) => {
    setTabs(tabKey);
    if (tabKey == 2) {
      navigate("/recipe/" + id + "/instructions");
    } else {
      navigate("/recipe/" + id + "/ingredients");
    }
  };

  return (
    <>
      {loading && <Loading />}
      {recipes && (
        <div className="flex w-full flex-col gap-8">
          <div className="hero w-full rounded-2xl bg-base-200">
            <div className="hero-content w-full flex-col-reverse gap-8 p-6 md:flex-row-reverse md:p-8">
              <img
                src={recipes.imageURL}
                className="h-56 w-full rounded-lg object-cover shadow-2xl md:w-1/2"
              />
              <div>
                <h1 className="text-5xl font-bold">{recipes.name}</h1>
              </div>
            </div>
          </div>

          <div role="tablist" className="tabs tabs-lifted tabs-lg">
            <a
              role="tab"
              className={"tab " + (tabs === 1 && "tab-active")}
              onClick={() => handleTabs(1)}
            >
              Ingredient
            </a>
            <a
              role="tab"
              className={"tab " + (tabs === 2 && "tab-active")}
              onClick={() => handleTabs(2)}
            >
              Steps
            </a>
          </div>
          <Outlet
            context={{
              instructions: recipes.steps,
              ingredients: recipes.ingredients,
            }}
          />
        </div>
      )}
      {error && (
        <Error message={error.message} explanation={error.description} />
      )}
      {error && console.log(error)}
    </>
  );
}

export default RecipePage;

/*eslint-disable no-unused-vars*/

import Hero from "../components/Hero";
import ProductGrid from "../components/ProductGrid";
import useFetchRecipes from "../hooks/useFetchRecipes";
import Loading from "../components/Loading";
import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";

function HomePage() {
  const [fetchRecipes, { recipes, loading, error }] = useFetchRecipes();
  const [_, setSearchParams] = useSearchParams();
  const [query, setQuery] = useState("");

  const handleSearchRecipe = (query) => {
    fetchRecipes(query);
    if (query) {
      setSearchParams({
        q: query,
      });
    }

    setQuery("");
  };

  useEffect(() => {
    fetchRecipes();
  }, []);

  return (
    <>
      <Hero
        handleSearchRecipe={handleSearchRecipe}
        query={query}
        setQuery={setQuery}
      />
      <h2 className="text-3xl font-bold text-neutral">Explore Recipe</h2>
      {recipes && <ProductGrid recipesData={recipes} />}
      {loading && <Loading />}
      {error && error}
    </>
  );
}

export default HomePage;
